from django.contrib.auth.models import AnonymousUser
from django.contrib.auth import get_user_model
from channels.db import database_sync_to_async
from channels.auth import AuthMiddleware
from channels.sessions import CookieMiddleware, SessionMiddleware
from django.conf import settings

User = get_user_model()


@database_sync_to_async
def get_user():
    try:
        user = User.objects.get(email=settings.ADMIN_USER_EMAIL)
        print(settings.ADMIN_USER_EMAIL)
    except Exception as e:
        print(e)
        return AnonymousUser()

    return user


class TokenAuthMiddleware(AuthMiddleware):
    async def resolve_scope(self, scope):
        scope['user']._wrapped = await get_user()


def TokenAuthMiddlewareStack(inner):
    return CookieMiddleware(SessionMiddleware(TokenAuthMiddleware(inner)))
