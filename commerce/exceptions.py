from rest_framework.exceptions import APIException
from rest_framework import status


class NotFoundException(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Not Found'


class BadRequestException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Redundant parameters passed.'


class NoItemsLeftException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'No items left.'
