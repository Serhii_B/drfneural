from datetime import datetime

import bcrypt
from django.contrib.auth.hashers import make_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from .models import User, Product
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from django.forms.models import model_to_dict


class NeuralObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(NeuralObtainPairSerializer, cls).get_token(user)

        token['username'] = user.email
        user.last_login = datetime.utcnow()
        user.save()
        return token


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True,
                                   validators=[UniqueValidator(queryset=User.objects.all())]
                                   )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    pin_code = serializers.CharField(required=True, max_length=4)

    class Meta:
        model = User
        fields = ['email', 'password', 'password2', 'name', 'last_name', 'pin_code', 'card_number']
        extra_kwargs = {
            'name': {'required': True},
            'email': {'required': True},
            'last_name': {'required': True},
            'card_number': {'required': True},
            'pin_code': {'required': True},
        }

    def validate_pin_code(self, value):
        hashed_pin = bcrypt.hashpw(value.encode('utf-8'), bcrypt.gensalt())
        return hashed_pin

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data.get('email'),
            name=validated_data.get('name'),
            last_name=validated_data['last_name'],
            card_number=validated_data['card_number'],
            pin_code=validated_data['pin_code'],
        )
        user.set_password(validated_data['password'])
        user.save()

        return user


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        field = ['name', 'slug', 'description', 'price', 'quantity']


class UserDetailsSerializer(serializers.Serializer):
    products_owned = ProductSerializer()

    class Meta:
        model = User
        fields = ['id', 'name', 'last_name', 'email', 'card_number', 'balance', 'products_owned']

    def get_products(self, obj):
        products_queryset = obj.get_owned_products()
        product_list = [model_to_dict(product) for product in products_queryset]
        [product.pop('owners') and product.pop('creator') for product in product_list]
        return product_list

    def get_products_total_price(self, obj):
        return obj.get_owned_products_total_price()

    def get_products_avarage_price(self, obj):
        return obj.get_all_products_avarage_price()

    def to_representation(self, instance):
        return {
            'name': instance.name,
            'last_name': instance.last_name,
            'email': instance.email,
            'card_number': instance.card_number,
            'owned_products': self.get_products(instance)
        }


class UpdateMeViewSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = ['email', 'name', 'last_name', 'pin_code', 'card_number', 'new_password']

    def update(self, instance, validated_data):
        new_password = validated_data.pop('new_password', None)
        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        if new_password:
            instance.password = make_password(new_password)

        instance.save()
        return instance


class UpdateUserByAdminSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=20, required=False)
    last_name = serializers.CharField(max_length=20, required=False)

    class Meta:
        model = User
        fields = ['name', 'last_name']


class CreateProductSerializer(serializers.ModelSerializer):
    creator = serializers.CharField(required=False)
    owner = serializers.CharField(required=False)

    class Meta:
        model = Product
        fields = '__all__'

    def validate_price(self, value):
        if value <= 0:
            raise serializers.ValidationError('Price must be greater than zero.')
        return value

    def validate_quantity(self, value):
        if value <= 0:
            raise serializers.ValidationError('Quantity must be greater than zero.')
        return value

    @staticmethod
    def conduct_notify(product: Product):
        data = dict()
        dict_modeled = model_to_dict(product)
        for item in ['name', 'creator']:
            data.update({item: dict_modeled[item]})
        data.update({'created': product.created.strftime(format="%Y-%m-%d %H:%M:%S")})

        message = {
            'type': 'send.notification',
            'product_data': data
        }
        channel_layer = get_channel_layer()

        # SENDING NOTIFICATION to the admin
        async_to_sync(channel_layer.group_send)(
            'admin', message
        )

    def create(self, validated_data):
        product = Product.objects.create(
            name=validated_data.get('name'),
            slug=validated_data.get('slug'),
            description=validated_data.get('description'),
            creator=self.context['request'].user,
            owners={self.context['request'].user.id: validated_data.get('quantity')},
            price=validated_data.get('price'),
            quantity=validated_data.get('quantity'),
        )
        self.conduct_notify(product)
        return product

    def to_representation(self, instance):
        user = instance.creator
        products = user.get_owned_products()
        product_number = products.count()
        product_list = [model_to_dict(product) for product in products]
        return {
            'products': product_list,
            'product_number': product_number,
            'total_owned_product_price': user.get_owned_products_total_price(products),
            'products_avarage_price': user.get_all_products_avarage_price(products),
            'total_owned_products_quantity': user.get_total_owned_products_quantity()
        }


class GetProductsAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'slug', 'description', 'price', 'created', 'quantity', 'owners']


class UpdateProductDetaulsSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    slug = serializers.CharField(required=False)
    price = serializers.FloatField(required=False)
    quantity = serializers.IntegerField(required=False)

    class Meta:
        model = Product
        fields = ['name', 'slug', 'description', 'price', 'quantity']

    def validate(self, attrs):
        if 'slug' in attrs and 'name' not in attrs:
            raise serializers.ValidationError({"slug": "Slug can be changed only together with name."})
        return attrs

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        instance.save()
        return instance
