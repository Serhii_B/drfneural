from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


@deconstructible
class CreditCardNumberValidator:
    def __call__(self, card_number):
        value = card_number.replace(" ", "")

        if not value.isdigit():
            raise ValidationError(
                _("Credit card must be digits only.")
            )

        if len(value) != 16:
            raise ValidationError(
                _("Invalid credit card length.")
            )

        # Luhn algorythm validation
        total = 0
        reverse_digits = value[::-1]
        for i, digit in enumerate(reverse_digits):
            if i % 2 == 1:
                doubled = int(digit) * 2
                total += doubled if doubled < 10 else doubled - 9
            else:
                total += int(digit)
        if total % 10 != 0:
            raise ValidationError(
                _("Invalid credit card number")
            )

        return value


@deconstructible
class PinCodeValidator:
    def __call__(self, pin_code):
        if not (pin_code.isdigit() and len(pin_code) == 4):
            raise ValidationError(_('PIN code must be exactly 4 digits.'))
