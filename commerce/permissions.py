from rest_framework.permissions import BasePermission


class IsOwnerOrAdminPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True

        return obj == request.user


class IsAdminUserPermission(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class IsOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsProductOwnerPermission(BasePermission):
    # def has_permission(self, request, view):
    #     return request.user.id in obj.owners

    def has_object_permission(self, request, view, obj):
        print(obj)
        return request.user.id in obj.owners
