from django.db import transaction
from django.forms import model_to_dict

from .exceptions import (
    NotFoundException,
    BadRequestException,
    NoItemsLeftException
)
from .models import User, Product
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import generics, status
from .permissions import (
    IsOwnerOrAdminPermission,
    IsAdminUserPermission,
    IsOwnerPermission,
)
from .serializers import (
    NeuralObtainPairSerializer,
    RegisterSerializer,
    UserDetailsSerializer,
    UpdateMeViewSerializer,
    UpdateUserByAdminSerializer,
    CreateProductSerializer,
    GetProductsAdminSerializer,
    UpdateProductDetaulsSerializer,
)


class ObtainTokenAPIView(TokenObtainPairView):
    permission_classes = (AllowAny, )
    serializer_class = NeuralObtainPairSerializer


class RegisterAPIView(generics.CreateAPIView):
    permission_classes = (AllowAny, )
    serializer_class = RegisterSerializer


class UserDetailsAPIView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    permission_classes = (IsOwnerOrAdminPermission, )
    serializer_class = UserDetailsSerializer

    def retrieve(self, request, *args, **kwargs):
        try:
            instance = self.queryset.get(pk=self.kwargs.get('pk'))
        except User.DoesNotExist:
            raise NotFoundException()
        if instance:
            serializer = self.serializer_class(instance)
            return Response(serializer.data)


class UserDetailsListAPIView(generics.ListAPIView):
    queryset = User.objects.filter(is_superuser=False)
    permission_classes = (IsAdminUserPermission, )
    serializer_class = UserDetailsSerializer


class UpdateMeAPIView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated, IsOwnerPermission)
    serializer_class = UpdateMeViewSerializer

    def get_object(self):
        return self.request.user


class UpdateUserByAdminAPIView(generics.UpdateAPIView):
    permission_classes = (IsAdminUserPermission, )
    serializer_class = UpdateUserByAdminSerializer

    def partial_update(self, request, *args, **kwargs):
        if not request.data:
            raise BadRequestException()
        try:
            user = User.objects.get(pk=self.kwargs.get('pk'))
        except User.DoesNotExist:
            raise NotFoundException()
        serializer = self.serializer_class(user, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class DeleteUserAPIView(generics.DestroyAPIView):
    permission_classes = (IsOwnerOrAdminPermission, )

    def delete(self, request, *args, **kwargs):
        try:
            user = User.objects.get(pk=self.kwargs.get('pk'))
        except User.DoesNotExist:
            raise NotFoundException()
        user.delete()
        return Response({'detail': 'User deleted.'}, status=status.HTTP_204_NO_CONTENT)


class CreateProductAPIView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, IsOwnerPermission)
    serializer_class = CreateProductSerializer


class GetAllProductsAPIView(generics.ListAPIView):
    queryset = Product.objects.all()
    permission_classes = (IsAdminUserPermission, )
    serializer_class = GetProductsAdminSerializer


class GetSpecificProductAPIView(generics.RetrieveAPIView):
    permission_classes = (IsOwnerOrAdminPermission, )
    serializer_class = GetProductsAdminSerializer

    def retrieve(self, request, *args, **kwargs):
        try:
            instance = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise NotFoundException()
        serializer = self.serializer_class(instance)
        return Response(serializer.data)


class UpdateProductDetailsAPIView(generics.UpdateAPIView):
    """
    Updates product details, slug can be updated only together with name
    """
    allowed_fields = ['name', 'slug', 'description', 'price', 'quantity']
    permission_classes = (IsAuthenticated, IsOwnerOrAdminPermission)
    serializer_class = UpdateProductDetaulsSerializer

    def partial_update(self, request, *args, **kwargs):
        if not request.data:
            raise BadRequestException()
        if not all([bool(key in self.allowed_fields) for key in request.data.keys()]):
            raise BadRequestException()

        try:
            product = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise NotFoundException()
        serializer = self.serializer_class(product, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        product = model_to_dict(product)
        product.pop('owners')
        product.pop('creator')
        return Response(product)


class DeleteProductItemAPIView(generics.UpdateAPIView):
    """
    Deletes single item of a Product, adds it to User.deleted_products and Prodcut.deleteded_quantity
    """
    permission_classes = (IsAuthenticated, IsOwnerPermission)

    def update(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise NotFoundException()

        user = request.user
        user_id = str(user.id)
        if user_id in product.owners:
            product.perform_item_delete(user_id)
            if user.deleted_products.get(str(product.id)):
                user.deleted_products[str(product.id)] += 1
                user.save()
            else:
                user.deleted_products[str(product.id)] = 1
                user.save()
        else:
            return Response({'details': 'You can not conduct this action'}, status=status.HTTP_403_FORBIDDEN)
        return Response({'details': 'Product item deleted.'}, status=status.HTTP_200_OK)


class DestroyProductAPIView(generics.DestroyAPIView):
    """
    Destroyes the Product object if admin_user or if only one user ownes product and there are no
    more available products to buy.
    """
    permission_classes = (IsAuthenticated, IsOwnerOrAdminPermission)

    def delete(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise NotFoundException()

        if request.user.is_superuser:
            product.delete()
            return Response({'details': 'Product deleted.'}, status=status.HTTP_204_NO_CONTENT)

        if str(request.user.id) in product.owners\
                and len(product.owners) == 1 \
                and not product.get_available_quantity() > 0:
            product.delete()
            return Response({'details': 'Product deleted.'}, status=status.HTTP_204_NO_CONTENT)

        return Response(
            {'details': 'Can not delete the Prodcut, maybe someone else owns it.'}, status=status.HTTP_200_OK
        )


class ConductProductPurchaseAPIView(APIView):
    """
    Check if the user can buy Product and makes one item asignment
    Prodcut.owners[request_user] += 1 item
    """
    @transaction.atomic()
    def post(self, request, pk):
        user = request.user
        try:
            product = Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            raise NotFoundException()

        if product.get_available_quantity() <= 0:
            raise NoItemsLeftException()

        if user.balance >= product.price:
            user.balance = user.balance - product.price

            if product.conduct_sell():
                user.save()
                return Response(
                    {'details': f'You have successfully purchased {product.name}'},
                    status=status.HTTP_200_OK
                )

        return Response({'details': 'Something went wrong'}, status=status.HTTP_400_BAD_REQUEST)
