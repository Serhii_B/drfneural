from django.urls import path
from .views import (
    ObtainTokenAPIView,
    RegisterAPIView,
    UserDetailsAPIView,
    UserDetailsListAPIView,
    UpdateMeAPIView,
    UpdateUserByAdminAPIView,
    DeleteUserAPIView,
    CreateProductAPIView,
    GetAllProductsAPIView,
    GetSpecificProductAPIView,
    UpdateProductDetailsAPIView,
    DeleteProductItemAPIView,
    DestroyProductAPIView,
    ConductProductPurchaseAPIView,
)
from rest_framework_simplejwt.views import TokenRefreshView

urlpatterns = [
    # Users
    path('login/', ObtainTokenAPIView.as_view(), name='obtain_token_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='refresh_token'),
    path('register/', RegisterAPIView.as_view(), name='register'),
    path('get_user_details/<pk>/', UserDetailsAPIView.as_view(), name='get_user_details'),
    path('get_users/', UserDetailsListAPIView.as_view(), name='get_users_details'),
    path('update_me/', UpdateMeAPIView.as_view(), name='update_me'),
    path('update_user/<pk>/', UpdateUserByAdminAPIView.as_view(), name='update_user_by_admin'),
    path('delete_user/<pk>/', DeleteUserAPIView.as_view(), name='delete_user'),

    # Products
    path('create_product/', CreateProductAPIView.as_view(), name='create_prooduct'),
    path('list_products/', GetAllProductsAPIView.as_view(), name='list_prooducts'),
    path('get_product/<pk>/', GetSpecificProductAPIView.as_view(), name='get_prooduct'),
    path('update_product_details/<pk>/', UpdateProductDetailsAPIView.as_view(), name='update_product'),

    # User delete product.item or product.quantity unit
    path('product_item_delete/<pk>/', DeleteProductItemAPIView.as_view(), name='delete_product_item'),
    # Delete/Destroy Product object
    path('delete_product/<pk>/', DestroyProductAPIView.as_view(), name='delete_product'),

    path('buy_product/<int:pk>/', ConductProductPurchaseAPIView.as_view(), name='buy_product'),

]
