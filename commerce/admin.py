from django.contrib import admin
from .models import User, Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'quantity']
    list_per_page = 25
    readonly_fields = ['slug', 'created']


class UserAdmin(admin.ModelAdmin):
    readonly_fields = ['pin_code', 'email']


admin.site.register(User, UserAdmin)
admin.site.register(Product, ProductAdmin)
