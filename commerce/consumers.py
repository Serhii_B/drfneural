from channels.generic.websocket import AsyncJsonWebsocketConsumer


class AdminNotifier(AsyncJsonWebsocketConsumer):
    groups = ['admin']

    async def connect(self):
        user = self.scope.get('user')
        if not user.is_superuser:
            await self.close()

        await self.channel_layer.group_add(
            group='admin',
            channel=self.channel_name
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            group='admin',
            channel=self.channel_name
        )
        await super().disconnect(code)

    async def send_notification(self, message):
        await self.send_json({
            'type': message.get('type'),
            'data': message.get('product_data')
        })

    async def receive(self, text_data=None, bytes_data=None, **kwargs):
        await self.channel_layer.group_send(
            'admin', {
                'type': 'send.notification',
                'message': text_data
             }
        )
