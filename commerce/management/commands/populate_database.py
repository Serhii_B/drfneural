import bcrypt
import json
import random

from django.db import transaction

from commerce.models import Product, User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Command to populate database.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--path', type=str, default='database_sample.json'
        )

    @transaction.atomic()
    def handle(self, *args, **options):
        path = options['path']
        with open(path) as json_data:
            data = json.load(json_data)

            users = []
            for user_data in data.get('users', []):
                user_data['pin_code'] = bcrypt.hashpw(user_data['pin_code'].encode('utf-8'), bcrypt.gensalt())
                user = User.objects.create(**user_data)
                users.append(user)
                self.stdout.write(self.style.SUCCESS(f'Successfully created user: {user}'))

            products = []
            for product_data in data.get('products', []):
                product_data.pop('id', None)
                product_data['creator'] = random.choice(users)
                product_data['owners'] = {str(product_data['creator']): product_data['quantity']}

                product = Product.objects.create(**product_data)
                products.append(product)
                self.stdout.write(self.style.SUCCESS(f'Successfully created product: {product}'))

        self.stdout.write(self.style.SUCCESS(f'Data population from {path} complete'))
