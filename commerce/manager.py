from django.contrib.auth.models import UserManager


class UserManager(UserManager):
    use_in_migrations = True

    def _create_user(self, email=None,
                     password=None, name=None, **extra_fields):

        is_superuser = extra_fields.pop('is_superuser', False)

        if len(email):
            email = self.normalize_email(email)
        else:
            email = None
        user = self.model(
            email=email,
            is_superuser=is_superuser,
            **extra_fields)
        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)
