from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

from .exceptions import NoItemsLeftException
from .manager import UserManager
from django.db import models
from django.utils.translation import gettext_lazy as _

from commerce.validators import CreditCardNumberValidator, PinCodeValidator


class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(_('Email'), unique=True, null=True)
    card_number = models.CharField(_('Credit Card'),
                                   validators=[CreditCardNumberValidator],
                                   unique=True
                                   )
    pin_code = models.CharField(_('Pin code'), validators=[PinCodeValidator])
    deleted_products = models.JSONField(name='deleted_products', default=dict)
    balance = models.DecimalField(null=True, max_digits=24, decimal_places=2)

    @property
    def is_staff(self):
        return self.is_superuser

    USERNAME_FIELD = 'email'
    objects = UserManager()

    def get_owned_products(self):
        return Product.objects.filter(owners__has_key=str(self.id))

    def get_total_owned_products_quantity(self):
        product_owners = Product.objects.filter(owners__has_key=str(self.id)).values('owners')
        return sum(single_owner['owners'][str(self.id)] for single_owner in product_owners)

    def get_owned_products_total_price(self, product_queryset=None):
        if product_queryset:
            return sum(product.price for product in product_queryset)
        return sum(product.price for product in self.get_owned_products())

    def get_all_products_avarage_price(self, product_queryset=None):
        if not product_queryset:
            product_queryset = self.get_owned_products()
        return sum(product.price for product in product_queryset)/product_queryset.count()

    def __str__(self):
        return f"User: {self.email} ."


class Product(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, null=True, blank=True)
    description = models.CharField(_('Product Description'), max_length=255, blank=True)
    price = models.DecimalField(_('Price'), max_digits=24, decimal_places=2)
    created = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(User, blank=False, null=False,
                                related_name='products_created', on_delete=models.CASCADE
                                )
    quantity = models.PositiveIntegerField(_('Products quantity'), blank=False, null=False)
    owners = models.JSONField(name='owners', default=dict)
    deleteded_quantity = models.PositiveIntegerField(_('Products quantity'), blank=True, default=0)

    def get_available_quantity(self):
        if not self.owners:
            return 0
        copy_owners = self.owners.copy()
        copy_owners.pop(str(self.id))
        owned_product_quantity = sum(map(int, copy_owners.values()))
        return self.quantity - owned_product_quantity - self.deleteded_quantity

    def perform_item_delete(self, owner_id: str):
        if self.deleteded_quantity >= self.quantity:
            if self.owners.get(owner_id) == 0:
                del self.owners[owner_id]
                self.save()
            raise NoItemsLeftException()
        else:
            self.deleteded_quantity += 1

        # should not be a case
        if self.owners[owner_id] <= 0:
            raise NoItemsLeftException()
        else:
            self.owners[owner_id] -= 1

        self.save()

    def conduct_sell(self):
        initial_owner = self.creator.id
        if self.owners.get(str(initial_owner))\
            and self.owners.get(str(initial_owner)) != 0:
            self.owners[str(initial_owner)] -= 1
            if self.owners.get(str(self.id)):
                self.owners[str(self.id)] += 1
            else:
                self.owners[str(self.id)] = 1
            self.save()
            return True
        return False

    def __str__(self):
        return f"{self.name} with quantity {self.quantity} for {self.price} per unit."
