DRF Neural

2 Models: Product and User with respective fields.
CRUD operations for both.

Websocket connection using djangochannels to notify amdin when the 
Product is created with responsible data.

Logic:
Returns to the User after Product is created the overall info about other Products 
that he owns etc.
Admin can see all Products, users only theirs.
Impossible to buy more products than there are.

Data:
Available command to populate_database based on the included json file.